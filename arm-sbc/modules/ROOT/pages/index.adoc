= ARM Single Board Computers
:page-layout: without_menu

Fedora fully supports  the Raspberry Pi 4 Single Board Computer. Additionally there are drivers for many alternative maker's models with the extent of support varying. Sometimes only the basic functions work, while, for example, accelerated graphics or Wifi connections are not available. The latter is due to the fact that some manufacturers do not offer open source drivers and a reimplmentation by the OSS community is not (yet) available.


xref:quick-docs::raspberry-pi.adoc[General information]
Details about different boards and a general introduction to installation 

xref:fedora-server::index.adoc[Fedora Server Edition]
Detailed installation information for Fedora Server Edition

