= Fedora Tools
:page-layout: without_menu
:page-aliases: quick-docs/how-to-edit-iptables-rules.adoc

Fedora includes a set of programs that enable users to customize and maintain a Fedora installation to meet individual needs. Fedora editions and spins each use these tools in a different way. Read the edition documentation to learn how to use these tools in a practical way. Here we provide a general overview and functional description for the tools that complements the edition documentation.

In general, Fedora is focused on its upstream projects, so also with the administration and management tools. This applies to both the software itself as well as the documentation. The Fedora documentation consequently focuses on describing Fedora specific customizations, for example, the choice of data directories or default values.

Fedora uses the following upstream projects.

== Instance Management
Anaconda::
https://github.com/rhinstaller/anaconda[Anaconda] is the installation program used by Fedora, Red Hat Enterprise Linux and some other distributions for installation of RPM file based systems, as Workstation or Server. 

* Upstream documentation: https://anaconda-installer.readthedocs.io/en/latest/[Welcome to Anaconda’s documentation!]
* Fedora documentation: https://fedoraproject.org/wiki/Anaconda[Anaconda] 
* However, the first source of information should definitely be the installation guidance of the Fedora variant in question.  

DNF::
The https://github.com/rpm-software-management/dnf[Dandified YUM (DNF)] is the software packages manager, used by Fedora to manage the RPM based software distribution.
* Upstream documentation: https://dnf.readthedocs.io/en/latest/[DNF, the next-generation replacement for YUM]
* Fedora related documentation: xref:quick-docs::dnf.adoc[Using the DNF software package manager]
* The documentation of the different Fedora variants may include additional, and more detailed information.

RPM::
There are various package managment systems to enable Linux distributions to build software from source into easily distributable packages und manage installation, updating and removing applications. Fedora uses the https://rpm.org/[RPM Package Manager].

* Upstream documentation: https://rpm-software-management.github.io/rpm/manual/[RPM Reference Manual]

rpm-ostree::
A hybrid image and package management system. It makes RPM packages available in an image based operating system. It uses the same repositories configured in `/etc/yum.repos.d` as dnf.

* Upstream documentation: https://coreos.github.io/rpm-ostree/[A true hybrid image/package system]


== Infrastructure Services

firewalld::
By default, Fedora uses __https://firewalld.org/[firewalld]__ as the high level user interface to manage a system's firewall. It abstracts a firewall, allows dynamic administration and simplifies the task significantly compared to iptables used by Fedora up to release 18. For specific purposes, _firewalld_ can also process iptables rules directly. Alternatively, _http://www.iptables.org[iptables]_ resp. its successor _nftable_ is still available and could be enabled instead of __firewalld__. However, this is neither recommended nor advisable.

* Upstream documentation: https://firewalld.org/documentation/[firewalld – A service daemon with D-Bus interface]
* Fedora specific documentation: xref:quick-docs::firewalld.adoc[Using firewalld]


OpenSSH:: 
Fedora uses https://www.openssh.com/portable.html[OpenSSH] to enable remote access to a system. It is automatically installed and savely configured. The system stores all entries into binary files and requires specific software to inspect log entries, but provides very useful inspection capabilities.

* Upstream documentation: https://www.openssh.com/manual.html[OpenSSH Manual Pages] 


systemd::
Fedora uses https://systemd.io/[systemd] for configuration and management of the services to be run on a given system. 

* Upstream documenation is highly technical and basically a set of https://www.freedesktop.org/software/systemd/man/systemd.directives.html[man pages]. It should hardly be helpful for most users. 

* Fedora documentation: xref:quick-docs::understanding-and-administering-systemd.adoc[Understanding and administering systemd]


// TigerVNC::
// Remote graphical access
// https://fedoraproject.org/wiki/Features/TigerVNC


== Monitoring and Automation

//System Monitoring Tools::
//short description TBD

Viewing and Managing Log Files::
Fedora uses systemd journald as the main tool to log all events. Basically, it replaces the rsyslog system. Fedora encourages all packages to redirect log messages to systemd-journald.
+
Additionally, Fedora continues to install rsyslog, which is also used by several software packages.

* Upstream documentation: https://www.freedesktop.org/software/systemd/man/systemd-journald.service.html[systemd-journald]
* Fedora documentation: xref:quick-docs::viewing-logs.adoc[Viewing logs in Fedora]



// Automating System Tasks::
//short description TBD

// OProfile::
//short description TBD

== Generic
Fedora specific information about the various administration and maintenance tools is available in the [citetitle]_xref:fedora:system-administrators-guide:index.adoc[Fedora System Administrator's Guide]_.
